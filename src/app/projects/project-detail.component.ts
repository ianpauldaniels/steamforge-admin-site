import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiComService } from '../services/api-com.service';
import { Observable } from 'rxjs';
import { formattedError } from '@angular/compiler';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html'
})
export class ProjectDetailComponent implements OnInit {

  // selected project id
  private pID;

  // Project variables
  project;
  projects;
  projects$: any = this.apiComService.getFromDB('project');

  // User Accounts variables
  accounts;
  accounts$: any = this.apiComService.getFromDB('account');

  // members variables
  // id_project, id_member
  members;
  members$: any = this.apiComService.getFromDB('member');

  // progress variables
  // id, created_in, summary, pdf_atached, id_project
  progresses;
  progresses$: any = this.apiComService.getFromDB('progress');

  // project partners variables
  // id_partner, id_project
  projectPartners;
  projectPartners$: any = this.apiComService.getFromDB('project-partner');

  // project photos variables
  // id,	created_in, id_project, name
  projectPhotos;
  projectPhotos$: any = this.apiComService.getFromDB('project-photo');

  // This project's properties
  projectMembers$: any;

  constructor(private route: ActivatedRoute,
    private apiComService: ApiComService) {
    this.pID = route.snapshot.paramMap.get('id');
  }

  ngOnInit() {

    this.getProjects();

    this.getInfoFromDB(this.accounts, this.accounts$);
    this.getInfoFromDB(this.members, this.members$);
    this.getInfoFromDB(this.progresses, this.progresses$);
    this.getInfoFromDB(this.projectPartners, this.projectPartners$);
    this.getInfoFromDB(this.projectPhotos, this.projectPhotos$);

    this.findProjectMembers();

    console.log(this.project);

    // Change Body Name
    const bodyClass = 'page-project';
    const body = document.querySelector('body');
    body.className = 'animsition ' + bodyClass;
  }

  private getInfoFromDB(localArray: Array<any>, localObservable$: Observable<any>) {
    localObservable$
      .subscribe(
        arg => {
          localArray = arg;
          console.log(arg);
        },
        err => console.log(err),
        () => console.log(localArray)
      );
  }

  private getProjects() {
    this.projects$
      .subscribe(
        arg => this.projects = arg,
        err => console.log(err),
        () => {
          this.findProject();
        }
      );
  }

  private findProject() {
    let flag = true;

    for (let i = 0; flag; i++) {
      console.log(this.projects[i]['id'] + ' === ' + this.pID + ' ?');

      if (this.pID == this.projects[i]['id']) {
        console.log('True!');
        this.project = this.projects[i];
        flag = false;
      } else {
        console.log('False');
      }
    }
  }

  private findProjectMembers() {
    const flag = false;

    for (let i = 0; flag == false; i++) {
      if (this.members[i]['id_project'] == this.projects[i]['id'] && this.members[i]['id_member'] == this.accounts[i]['id'] ) {
        this.projectMembers$ += this.accounts[i];
        console.log(this.projectMembers$);
      } else {
        console.log('TEST ' + i);
      }
    }
  } // this.findProjectMembers
}
