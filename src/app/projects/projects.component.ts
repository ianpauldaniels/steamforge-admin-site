import { Component, OnInit, NgModule } from '@angular/core';
import { ApiComService } from '../services/api-com.service';

@NgModule({})

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html'
})
export class ProjectsComponent implements OnInit {

  constructor(private apiComService: ApiComService) {}

  projects: any;
  projects$: any = this.apiComService.getFromDB('project');

  ngOnInit() {
    // Change Body Name
    // const bodyClass = 'app-projects site-menubar-hide';
    const bodyClass = 'app-projects';
    const body = document.querySelector('body');
    body.className = 'animsition ' + bodyClass;


    this.getProjects();

  }
  getProjects(): any {
    this.projects$
      .subscribe(
        arg => this.projects = arg,
        err => console.log(err),
        () => console.log(this.projects)
      );

  }

}

