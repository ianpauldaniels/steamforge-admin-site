import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiComService {

  constructor(private http: HttpClient) { }

  // Test Fetch
  fetchGithub() {
    const obs = this.http.get('https://api.github.com/users/ianpauldaniels');
    obs.subscribe(
      response => console.log(response)
    );
  }

  // General Get
  getFromDB2(tableName: string) {
    let res;
    this.http.get('http://localhost:3000/' + tableName)
      .subscribe( // subscribe to observable
        response => {
          res = response;
          console.log(res);
        }
      );
    return res;
  }

  getFromDB(tableName: string): Observable<Object> {
    return this.http.get('http://localhost:3000/' + tableName);
  }

  getAccounts() {
    this.getFromDB('account');
  }

  getAccesses() {
    this.getFromDB('acces');
  }

  getMembers() {
    this.getFromDB('member');
  }

  getMemberPhotos() {
    this.getFromDB('member-photo');
  }

  getMetrics() {
    this.getFromDB('metrics');
  }

  getPartners() {
    this.getFromDB('partner');
  }

  getPermits() {
    this.getFromDB('permit');
  }

  getProgress() {
    this.getFromDB('progress');
  }

  getProjects() {
    this.getFromDB('project');
  }

  getProjectMembers() {
    this.getFromDB('project-member');
  }

  getProjectPartners() {
    this.getFromDB('project-partner');
  }

  getProjectPhotos() {
    this.getFromDB('project-photo');
  }

  getRoles() {
    this.getFromDB('rol');
  }
}
