import { TestBed } from '@angular/core/testing';

import { ApiComService } from './api-com.service';

describe('ApiComService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiComService = TestBed.get(ApiComService);
    expect(service).toBeTruthy();
  });
});
