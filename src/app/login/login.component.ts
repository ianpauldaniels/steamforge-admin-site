import { Component, OnInit } from '@angular/core';
import { Globals } from '../globals';
import { Router } from '@angular/router';
import { ApiComService } from '../services/api-com.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  pageTitle = Globals.title;
  logo = '../../assets/SFLogos/Steam sticker 1transparent 2.png';

  constructor(
    private router: Router,
    private apiComService: ApiComService) { }

  users$: any = this.apiComService.getFromDB('account');
  userAccounts: any;

  getAccounts() {
    this.users$.subscribe(
      arg => this.userAccounts = arg, // Main Argument
      err => console.log(err), // Error
      () => console.log(this.userAccounts) // Complete
    );
  }

  ngOnInit() {
    // Change Body Name
    const bodyClass = 'page-login-v3 layout-full';
    const body = document.querySelector('body');
    body.className = 'animsition ' + bodyClass;

    // hide navbar
    document.querySelector('nav').style.visibility = 'hidden';
    body.style.backgroundColor = 'blue';

    this.getAccounts();
  }

  onSubmit() {
    Globals.userLoggedIn = true;
    this.router.navigate(['/home']);
    document.querySelector('nav').style.visibility = 'visible';

  }

}
