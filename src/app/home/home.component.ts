import { Component, OnInit, NgModule } from '@angular/core';
import * as datajson from '../../assets/json/data';
import { Router } from '@angular/router';
import { ApiComService } from '../services/api-com.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit {
  constructor(
    private router: Router,
    private apiComService: ApiComService) { }

  projects$: any = this.apiComService.getFromDB('project');
  // project array to be filled with loop from data.json
  projects: any;
  numProjects;

  // Constants to reduce data.json url
  dataProjects = datajson.default.property.data.projects;

  ngOnInit() {
    this.getProjects();

    // Change Body Name
    const bodyClass = 'dashboard';
    const body = document.querySelector('body');
    body.className = 'animsition ' + bodyClass;
  }

  getProjects() {
    this.projects$
      .subscribe(
        arg => this.projects = arg,
        err => console.log(err),
        () => console.log(this.projects)
      );
  }
}
