import { Component, OnInit } from '@angular/core';
import * as datajson from '../assets/json/data';

console.log(datajson.default.property.data);

@Component({
  selector: 'app-root',
  template: `
    <app-navbar></app-navbar>
    <router-outlet></router-outlet>
  `
})
export class AppComponent implements OnInit {
  title = 'SteamForge Admin Site';
  logo = '../assets/SFLogos/Steam forge 2 transparent 2.png';

  constructor() { }

  ngOnInit() {
    // Change Body Name
    const bodyClass = 'dashboard';
    const body = document.querySelector('body');
    body.className = 'animsition ' + bodyClass;
  }
}
