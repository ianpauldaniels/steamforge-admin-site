import { Component, OnInit } from '@angular/core';
import { Globals } from '../globals';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {
  logo: string;
  user = Globals.user;

  constructor() {}

  ngOnInit() {
    this.logo = '../../assets/SFLogos/Steam forge 2 transparent 2.png';
  }

}
