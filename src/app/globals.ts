// globals.ts
import { Injectable } from '@angular/core';
import * as datajson from '../assets/json/data';

@Injectable()
export class Globals {
  static userLoggedIn = false;
  static title = 'SteamForge Admin Site';
  static logo = '../assets/SFLogos/Steam sticker 1transparent 2.png';

  static user = datajson.default.property.user;
  static projects = datajson.default.property.data.projects;
}
