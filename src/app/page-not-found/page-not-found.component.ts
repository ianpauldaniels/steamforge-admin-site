import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';


@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html'
})
export class PageNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    
    // Change Body Name
    const bodyClass = 'page-error page-error-404 layout-full';
    var body = document.querySelector('body');
    body.className = 'animsition ' + bodyClass;
  }

}
