import { Component, OnInit } from '@angular/core';
import * as datajson from '../../assets/json/data';
import { ApiComService } from '../services/api-com.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {
  userID: string;
  users$: any = this.apiComService.getFromDB('account');
  user: any;
  users: any;

  constructor(private route: ActivatedRoute,
    private apiComService: ApiComService) {
    this.userID = route.snapshot.paramMap.get('id');
  }

  // user = datajson.default.property.user;

  ngOnInit() {

    this.getUsers();

    console.log(this.user);

    // Change Body Name
    const bodyClass = 'page-profile-v2';
    const body = document.querySelector('body');
    body.className = 'animsition ' + bodyClass;
  }

  getUser() {
    try {
      for (let i = 0, flag = false; flag == false; i++) {
        if (this.userID == this.users[i].id) {
          this.user = this.users[i];
          flag = true;
          alert(this.user.name + ' profile loading.');
        } else {
          console.log('Checked user #' + i);
          console.log(this.users[i]);
        }
      }
    } catch (err) {
      alert(err);
    }
  }

  getUsers(): any {
    this.users$
    .subscribe(
      arg => this.users = arg,
      err => console.log(err),
      // On subscribe complete
      () => {
        console.log(this.users);
        this.getUser();
      }
    );
  }

}
