import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ProjectsComponent } from './projects/projects.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { ProjectDetailComponent } from './projects/project-detail.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { LogInGuardGuard } from './guards/log-in-guard.guard';
import { NavbarComponent } from './navbar/navbar.component';
import { UserService } from './services/user.service';

import { Globals } from './globals';
import { PostgreSQLdb } from '../server/_DatabasePostgres';

const appRoutes: Routes = [
  { path: 'profile/:id', component: ProfileComponent },
  { path: 'projects/:id', component: ProjectDetailComponent},
  { path: 'projects', component: ProjectsComponent },
  { path: 'home',
      component: HomeComponent,
      canActivate: [ LogInGuardGuard ]},
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ProjectsComponent,
    PageNotFoundComponent,
    HomeComponent,
    ProjectDetailComponent,
    LoginComponent,
    ProfileComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    HttpClientModule
  ],
  providers: [ Globals, PostgreSQLdb, UserService ],
  bootstrap: [AppComponent]
})
export class AppModule {
// Sequelize test start **************************************************

// import { Sequelize } from 'sequelize-typescript';

// const sequelize =  new Sequelize({
//   database: 'steamforgeDB',
//   dialect: 'postgreSQL',
//   username: 'iandaniels',
//   password: 'iandaniels',
//   host: '127.0.0.1',
//   port: 53763 // http://127.0.0.1:53763/?key=ebe2a6f7-465a-46b3-8831-78acf3af75d7
// });
// const sequelize = new Sequelize('postgres://localhost:5432/steamforgeDB');

// sequelize.authenticate().then(() => {
//  console.log('Connection has been established successfully.');
// })
// .catch(err => {
//   console.error('Unable to connect to the database:', err);
// });

// Sequilize test end **************************************************

}
