// import * as pg from 'pg';
import * as pg_1 from 'pg';
import { OnInit } from '@angular/core';

export class PostgreSQLdb implements OnInit {

    connectionString =
        // 'postgres://iandaniels:iandaniels@127.0.0.1:52143/steamforgeDB';
        // 'postgres://127.0.0.1:52143/steamforgeDB';
        'postgres://iandaniels:iandaniels@localhost/steamforgeDB';

    pgClient = new pg_1.Client(this.connectionString);

    constructor() { }

    ngOnInit(): void {
        let query;

        this.pgClient.connect().then(
            (res) => {
                query = this.pgClient.query('SELECT * from account');
                console.log(query);
                console.log(res);
            },
            (err) => {
                console.log(err);
            });
    }
}
