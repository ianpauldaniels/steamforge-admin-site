"use strict";
exports.__esModule = true;
var sequelize_typescript_1 = require("sequelize-typescript");
var config_1 = require("../../config"); // DB connection parameters
exports.sequelize = new sequelize_typescript_1.Sequelize({
    database: config_1.dbconfig.database,
    dialect: config_1.dbconfig.dialect,
    username: config_1.dbconfig.username,
    password: config_1.dbconfig.password,
    host: config_1.dbconfig.host,
    port: config_1.dbconfig.port
});
exports.sequelize.authenticate().then(function () {
    console.log('Connected to DB');
})["catch"](function (err) {
    console.log(err);
});
